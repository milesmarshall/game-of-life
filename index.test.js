/* rules
   x < 2 = 0
   x >= 2 = x
   x > 3 = 0
   0 == 3 = x
*/

/*
0,0 = 0
0,1 = 0
0,2 = 0
0,3 = X
0,4 = 0
1,0 = 0
1,1 = 0
1,2 = 0
1,3 = X
1,4 = X
*/

const doTest = require('./index');

//0,0
test('0,0', () => {
  expect(doTest(0, 0)).toBe('0');
});

//0,1
test('0,1', () => {
  expect(doTest(0, 1)).toBe('0');
});

//0,2
test('0,2', () => {
  expect(doTest(0, 2)).toBe('0');
});

//0,3
test('0,3', () => {
  expect(doTest(0, 3)).toBe('X');
});

//0,4
test('0,4', () => {
  expect(doTest(0, 4)).toBe('0');
});

//1,0
test('1,0', () => {
  expect(doTest(1, 0)).toBe('0');
});

//1,1
test('1,1', () => {
  expect(doTest(1, 1)).toBe('0');
});

//1,2
test('1,2', () => {
  expect(doTest(1, 1)).toBe('0');
});

//1,3
test('1,3', () => {
  expect(doTest(1, 3)).toBe('X');
});

//1,4
test('1,4', () => {
  expect(doTest(1, 4)).toBe('X');
});
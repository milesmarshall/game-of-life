var gridHeight = 0;
var gridWidth = 0;
var ticks = 0;
var counter = 0;
var alive = 0;
var dead = 0;
var test1 = 0;
var test2 = 0;
var test3 = 0;
var test4 = 0;

var theGrid = "";
var mirrorGrid = "";

function go() {
  var errors = false;

  gridHeight = 0;
  gridWidth = 0;
  ticks = 0
  counter = 0;
  alive = 0;
  dead = 0;
  test1 = 0;
  test2 = 0;
  test3 = 0;
  test4 = 0;

  // quick check that user did enter a value
  if (document.getElementById("height").value < 10)
  {
    alert ('The height of the grid needs to be bigger than that');
    document.getElementById("height").focus();
    errors = true;
  }
  if (document.getElementById("width").value < 10)
  {
    alert ('The width of the grid needs to be bigger than that');
    document.getElementById("width").focus();
    errors = true;
  }
  if (document.getElementById("ticks").value < 10)
  {
    alert ('We need more iterations than that');
    document.getElementById("ticks").focus();
    errors = true;
  }

  if (!errors){
    document.getElementById("iterations").style = "block";
    document.getElementById("tests").style = "block";

    theGrid = createArray(document.getElementById("width").value);
    mirrorGrid = createArray(document.getElementById("width").value);

    gridHeight = document.getElementById("height").value;
    gridWidth = document.getElementById("width").value;
    ticks = document.getElementById("ticks").value;

    // randomise grid
    fillRandom();

    //start
    window.requestAnimationFrame(tick);
  }
}

function tick() {
    counter++;

    document.getElementById("iterations").innerHTML = "iterations: " + counter;

    drawGrid();
    updateGrid();

    // if the number is less, run it again
    if (counter < ticks) {
            outputGrid();
      window.requestAnimationFrame(tick);
    }
};

//creates a 2 dimensional array of required height
function createArray(rows) {
    var arr = [];
    for (var i = 0; i < rows; i++) {
        arr[i] = [];
    }
    return arr;
}

//fill the grid randomly
function fillRandom() {
    for (var j = 0; j < gridHeight; j++) { //iterate through rows
        for (var k = 0; k < gridWidth; k++) { //iterate through columns
            var rawRandom = Math.random(); //get a raw random number
            var improvedNum = (rawRandom * 2); //convert it to an int
            var randomBinary = Math.floor(improvedNum);

            if (randomBinary === 1) {
                theGrid[j][k] = 1;
            } else {
                theGrid[j][k] = 0;
            }
        }
    }
}

//draw the contents of the grid
function outputGrid() {
    var str = '\n';
    for (var j = 1; j < gridHeight; j++) { //iterate through rows
        for (var k = 1; k < gridWidth; k++) { //iterate through columns
            if (theGrid[j][k] === 1)
                str += '1';
            else
                str += '0';
        }
        str += '\n';
    }

    str += '\n';
    console.log(str);
}

function drawGrid() { //draw the contents of the grid onto a canvas
  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");

  ctx.clearRect(0, 0, 400, 400);

  //iterate through rows
  for (var j = 1; j < gridHeight; j++) {
      //iterate through columns
      for (var k = 1; k < gridWidth; k++) {
          if (theGrid[j][k] === 1) {
              ctx.beginPath();

              ctx.moveTo(j - 5, k - 5);
              ctx.lineTo(j + 5, k + 5);
              ctx.stroke();

              ctx.moveTo(j + 5, k - 5);
              ctx.lineTo(j - 5, k + 5);
              ctx.stroke();
          }
      }
  }
}

//perform one iteration of grid update
function updateGrid() {
    for (var j = 1; j < gridHeight - 1; j++) { //iterate through rows
        for (var k = 1; k < gridWidth - 1; k++) { //iterate through columns
            var totalCells = 0;

            //add up the total values for the surrounding cells
            totalCells += theGrid[j - 1][k - 1]; //top left
            totalCells += theGrid[j - 1][k]; //top center
            totalCells += theGrid[j - 1][k + 1]; //top right

            totalCells += theGrid[j][k - 1]; //middle left
            totalCells += theGrid[j][k + 1]; //middle right

            totalCells += theGrid[j + 1][k - 1]; //bottom left
            totalCells += theGrid[j + 1][k]; //bottom center
            totalCells += theGrid[j + 1][k + 1]; //bottom right

            //doTest(theGrid[j][k], totalCells);

            //apply the rules to dead cell
            if (theGrid[j][k] === 0) {
                dead++;
                document.getElementById("dead").innerHTML = "dead: " + dead;
                switch (totalCells) {
                    case 3:
                      test4++;
                      document.getElementById("test4").innerHTML = "test4: " + test4;
                        mirrorGrid[j][k] = 1; //if cell is dead and has 3 neighbours, test case 4: switch it on; 0 == x
                        break;
                    default:
                        mirrorGrid[j][k] = 0; //otherwise leave it dead
                }
            } else if (theGrid[j][k] === 1) { //apply rules to living cell
                alive++;
                document.getElementById("alive").innerHTML = "alive: " + alive;
                switch (totalCells) {
                    case 0:
                    case 1:
                        test1++;
                        document.getElementById("test1").innerHTML = "test1: " + test1;
                        mirrorGrid[j][k] = 0; //die of lonelines; test case 1: x < 2
                        break;
                    case 2:
                    case 3:
                      test2++;
                      document.getElementById("test2").innerHTML = "test2: " + test2;
                        mirrorGrid[j][k] = 1; //carry on living; test case 2: x > 2
                        break;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                      test3++;
                      document.getElementById("test3").innerHTML = "test3: " + test3;
                        mirrorGrid[j][k] = 0; //die of overcrowding; test case 3: x > 3
                        break;
                    default:
                        mirrorGrid[j][k] = 0; //

                }
            }
        }
    }

    for (var j = 0; j < gridHeight; j++) { //iterate through rows
        for (var k = 0; k < gridWidth; k++) { //iterate through columns
            theGrid[j][k] = mirrorGrid[j][k];
        }
    }
}

function doTest(state, totalCells){
  //test 1
  if ((state == 1) && (totalCells < 2))
    return '0';

  //test 2
  else if ((state == 1) && (totalCells >= 2))
    return 'X';

  //test 3
  else if ((state == 1) && (totalCells > 3))
    return 'X';

  //test 4
  else if ((state == 0) && (totalCells == 3))
    return 'X';

  else
    return '0';
}

function sum(a, b) {
  return a + b;
}
module.exports = sum;
module.exports = doTest;